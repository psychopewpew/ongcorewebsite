<html>
    <head>
        <link rel="stylesheet" type="text/css" href="style.css?v=1">
    </head>
    <body>
        <h1>This is a scuffed fan site for JonathanOng</h1>
        <div class="section"> <!--Twitch-->
            <h2>Become a part of the Orange Jacket Brigade (OJB) on Twitch</h2>
            Jonathan streams on Twitch three days a week. 
            The start times are:
            <p>
                <table>
                    <tr>
                        <th>Day</th>
                        <th>Start time (AEST/AEDT)</th>
                        <th>Start time (UTC (Oct - Mar))</th>
                        <th>Start time (UTC (Apr - Sep))</th>
                    </tr>
                    <tr>
                        <td>Tuesday</td>
                        <td>12:00 / 12pm</td>
                        <td>1:00 / 1am</td>
                        <td>2:00 / 1am</td>
                    </tr>
                    <tr>
                        <td>Thursday</td>
                        <td>12:00 / 12pm</td>
                        <td>1:00 / 1am</td>
                        <td>2:00 / 1am</td>
                    </tr>
                    <tr>
                        <td>Saturday</td>
                        <td>23:30 / 11:30pm</td>
                        <td>12:30 / 12:30pm</td>
                        <td>13:30 / 1:30pm</td>
                    </tr>
                </table>
            </p>
            Link to the stream: <a href="https://www.twitch.tv/jonathanong">https://www.twitch.tv/jonathanong</a>
        </div>
        <div class="section"> <!-- Discord -->
            <h2>Want to chat with us, even after stream? Join our discord!</h2>
            Get notifications when a stream starts, access to all VODs and chat with us.
            Click on the invite to get access now:</br>
            <div class="logolink"><a href="https://discord.gg/jonathanong"><img class="logo" src="static/Discord.svg" /></a></div>
        </div>
        <div class="section"> <!-- Highlights -->
            <h2>Jon is offline and want to check him out? Try the highlights!</h2>
            There is a full YouTube channel full of highlights of recent performances.
            Stick for a full video, it may start slow but ramps up until the end!
            Go to YouTube for the highlights:
            <div class="logolink"><a href="https://www.youtube.com/c/JonathanOng"><img class="logo" src="static/Youtube.svg" /></a></div>
        </div>
        <div class="section"> <!-- Patreon -->
            <h2>Want to support Jon, but in a more direct way? Patreon got you!</h2>
            Get access to an exclusive piece of music every month as well as the subscriber only section in discord!
            Support Jon here:
            <div class="logolink"><a href="https://www.patreon.com/jonathanong77"><img class="logo" src="static/Patreon.svg" /></a></div>
        </div>
        <div class="section"> <!-- Twitter -->
            <h2>Prefer short texts? Jon got you covered on Twitter!</h2>
            Get updates on the life of Ong in the shortest form you can find: a few words and a picture!
            Follow Jon on Twitter:
            <div class="logolink"><a href="https://twitter.com/JonathanOng77"><img class="logo" src="static/Twitter.svg" /></a></div>
        </div>
        <div class="section"> <!-- Music -->
            <h2>Listen to Jon even if he is offline</h2>
            Jon released two albums and one single track already!
            Find his own creations on 
                <a href="https://www.youtube.com/channel/UC0oAi2Suigb5ocM9SB1lEUA">YouTube</a>, 
                <a href="https://open.spotify.com/album/1RzLbBNRznsO8827MHgclL?si=lzRcyFRNQtWAxNb25Jwyag">Spotify</a>,
                <a href="https://music.apple.com/us/artist/jonathan-ong/1521103847">Apple Music</a> or 
                <a href="https://ongcore.bandcamp.com/">Bandcamp!</a>.
        </div>
        <div class="section"> <!-- Merch -->
            <h2>Want to wear your own shirt of the OBJ? Merch available!</h2>
            T-Shirt, Sweatshirt or Hoodie are all available at <a href="https://jonathanong.threadless.com/">https://jonathanong.threadless.com/</a>
        </div>
        <div class="section"> <!-- Contact - Business enquires -->
            <h2>Want to strike a business deal with Jon</h2>
            Reach out to Letofski via e-mail at <span class="mail">leto AT gamertalent.com.au</span>
        </div>
    </body>
</html>